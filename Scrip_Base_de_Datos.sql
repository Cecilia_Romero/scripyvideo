USE [ProyectoFinal]
GO
/****** Object:  Table [dbo].[Finalizar]    Script Date: 04/11/2019 22:31:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Finalizar](
	[Pago] [varchar](50) NULL,
	[Cantidad] [float] NULL,
	[Persona] [varchar](50) NULL,
	[id_Pagado] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_id_Pagado] PRIMARY KEY CLUSTERED 
(
	[id_Pagado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ficha]    Script Date: 04/11/2019 22:31:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ficha](
	[Nom_per] [varchar](50) NULL,
	[Apelli_per] [varchar](50) NULL,
	[correo] [varchar](50) NULL,
	[num_telefono] [int] NULL,
	[id_ficha] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_id_ficha] PRIMARY KEY CLUSTERED 
(
	[id_ficha] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Carreras]    Script Date: 04/11/2019 22:31:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Carreras](
	[Nom_Carrera] [varchar](50) NULL,
	[id_carreras] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_id_carreras] PRIMARY KEY CLUSTERED 
(
	[id_carreras] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Carrera]    Script Date: 04/11/2019 22:31:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Carrera](
	[Nom_ca] [varchar](50) NULL,
	[id_Carrera] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_id_Carrera] PRIMARY KEY CLUSTERED 
(
	[id_Carrera] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Aspirante]    Script Date: 04/11/2019 22:31:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aspirante](
	[Nom_aspirante] [varchar](50) NULL,
	[account] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[id_aspirante] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_id_aspirante] PRIMARY KEY CLUSTERED 
(
	[id_aspirante] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[LlenarFicha]    Script Date: 04/11/2019 22:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[LlenarFicha] 



@Nom_per varchar(50), @Apelli_per varchar(50), @correo varchar(50), @num_telefono int 


as 


if NOT EXISTS (SELECT Nom_per FROM Ficha where Nom_per=@Nom_per)
insert into Ficha (Nom_per, Apelli_per, correo, num_telefono) 
values (@Nom_per, @Apelli_per, @correo, @num_telefono)

else

update Ficha set Nom_per=@Nom_per, Apelli_per=@Apelli_per, correo=@correo, num_telefono=@num_telefono
GO
/****** Object:  StoredProcedure [dbo].[Llenadodepago]    Script Date: 04/11/2019 22:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Llenadodepago]

@Pago varchar(50), @Cantidad float, @Persona varchar(50)

as

if not exists (SELECT Pago from Finalizar where Pago=@Pago)
insert into Finalizar  (Pago, Cantidad, Persona) 
values (@Pago, @Cantidad, @Persona)

else

update Finalizar set Pago=@Pago, Cantidad=@Cantidad, Persona=@Persona
GO
/****** Object:  StoredProcedure [dbo].[EliminarDatos]    Script Date: 04/11/2019 22:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarDatos]

@id_ficha int 

as

delete from Ficha where id_ficha=@id_ficha
GO
